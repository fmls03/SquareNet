import os
from tkinter import N 
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import *

# import from other files
from _auth import *
from _create_post import *
from _home import *
from _redirecting import *

key = os.urandom(256)
key = str(key)


app = Flask(__name__) # Modulo flask
app.config['SECRET_KEY'] = key
app.config ['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://fmlspi:Schipilliti03!@93.51.26.126/SquareNet'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


# blueprints
app.register_blueprint(auth_bp)
app.register_blueprint(createPost_bp)
app.register_blueprint(redirecting_bp)
app.register_blueprint(home_bp)


db = SQLAlchemy(app)


# db classes
class User(db.Model):
    __tablename__ = 'user'
    email = db.Column(db.String(255), unique = True)
    username = db.Column(db.String(255), primary_key = True, unique = True)
    passw = db.Column(db.String(255))
    posts = db.relationship('Post', backref=('user'))

    def __init__(self, email, username, passw):
        self.email = email
        self.username = username
        self.passw = passw


class Forum(db.Model):
    __tablename__ = 'forum'
    id_forum = db.Column(db.Integer, primary_key = True, autoincrement = True)
    title = db.Column(db.String(255))
    n_topics = db.Column(db.Integer)
    n_posts = db.Column(db.Integer)
   
    topics = db.relationship('Topic', backref=('forum'))

    def __init__ (self, title, n_topics, n_posts):
        self.title = title
        self.n_topics = n_topics
        self.n_posts = n_posts



class Topic(db.Model):
    __tablename__ = 'topic'
    id_topic = db.Column(db.Integer, primary_key = True, autoincrement = True)
    title = db.Column(db.String(255))
    n_posts = db.Column(db.Integer)
    creator = db.Column(db.String(255), db.ForeignKey('user.username'))
    id_forum = db.Column(db.Integer, db.ForeignKey('forum.id_forum'))

    posts = db.relationship('Post', backref=('topic'))

    def __init__ (self, title, n_posts, creator, id_forum):
        self.title = title
        self.n_posts = n_posts
        self.creator = creator
        self.id_forum = id_forum



class Post(db.Model):
    __tablename__ = 'post'
    id_post = db.Column(db.Integer, primary_key = True, autoincrement = True)
    title = db.Column(db.String(255))
    description = db.Column(db.Text)
    insertTime = db.Column(db.DateTime)
    n_likes = db.Column(db.Integer)
    publisher = db.Column(db.String(255), db.ForeignKey('user.username'))
    id_topic = db.Column(db.Integer, db.ForeignKey('topic.id_topic'))

    def __init__(self, title, description, insertTime, n_likes, publisher, id_topic):
        self.title = title
        self.description = description
        self.insertTime = insertTime
        self.n_likes = n_likes
        self.publisher = publisher
        self.id_topic = id_topic

def clean_session():
    return redirect('/logout')

if __name__ == "__main__":
    app.run("localhost", 5000, debug=True)
    clean_session()
