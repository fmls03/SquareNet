from flask import session, render_template,Blueprint, redirect
from sqlalchemy import *
home_bp = Blueprint('home_bp', __name__)

import _app


@home_bp.route('/Home')
def Home():
    forums = ""
    if not session.get('logged_in'):
        return redirect('/logout')
    else:
        forums = _app.Forum.query.all()   
    return render_template('home.html', session=session, forums = forums)